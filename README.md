
---

### Hi there 👋 I'm Neko7sora! ✨ねこそら(neko-sora)

<table>
  <tr>
    <td><img src="https://github-readme-stats.api.dev.neko7sora.site/api?username=Neko7sora&count_private=true&show_icons=true&bg_color=ffffff00&title_color=5094f0&text_color=009a23&icon_color=fb7603&hide_border=true" /></td>
    <td><img src="https://github-readme-stats.api.dev.neko7sora.site/api/top-langs/?username=Neko7sora&layout=compact&count_private=true&bg_color=ffffff00&title_color=5094f0&text_color=009a23&icon_color=fb7603&langs_count=10&hide_border=true" /></td>
  </tr>
</table>

---

⚠Gitなりすまし対策のため、**GPG署名付き**のコミットをしています。デジタル署名が正しかった場合「Verified」マークが付きます。

<!--<img align="right" src="https://github.com/Neko7sora/Neko7sora/blob/main/signature.png?raw=true" />-->

デジタル署名とは、送信されてきたデータが間違いなく本人のものであるのかを証明するのための技術です。

もし、「Verified」マークが付いていなかった場合はなりすましの可能性があります。
<!--```js
if(github.Verified.name == "Neko7sora") return true
```
-->
---

 ### GitHub Gist feed
  Timezone: `Asia/Tokyo`
<!-- gist feed start -->
- 2022/06/21 - [iOS cloudflare DNS profile](https://gist.github.com/Neko7sora/7e6654174a89429881363cb0379e255f)
- 2021/12/16 - [DevTools shortcuts disable](https://gist.github.com/Neko7sora/5f556ea281743144d26be54fb09a29f3)
- 2021/10/31 - [keybase.md](https://gist.github.com/Neko7sora/5a8e656283e7e51398434310f5bb9e8c)
- 2021/10/05 - [Text art](https://gist.github.com/Neko7sora/5dabc3489bad7338b5aab453fe805761)
- 2021/10/05 - [Neko7sora](https://gist.github.com/Neko7sora/bbd2772504f0eef2d310edf8df66c227)
<!-- gist feed end -->

---

 ### Articles feed
 Timezone: `Asia/Tokyo` Articles: Blogger, Qiita, Revue, note, Zenn
<!-- articles feed start -->
- 2022/02/20 - [ #Zenn Articles テクノロジーに関するTwitterコミュニティを作成した](https://www.getrevue.co/profile/Neko7sora/issues/zenn-articles-twitter-1013995)
- 2022/02/19 - [テクノロジーに関するTwitterコミュニティを作成した](https://zenn.dev/neko7sora/articles/ec72545f2d7d7a)
- 2022/01/31 - [#Zenn Scrap yarn v3がyarn v1と誤認識してしまう](https://www.getrevue.co/profile/Neko7sora/issues/zenn-scrap-yarn-v3-yarn-v1-1000457)
- 2022/01/24 - [#Zenn Scrap monorepo リスト](https://www.getrevue.co/profile/Neko7sora/issues/zenn-scrap-monorepo-966048)
- 2022/01/02 - [2021年の記録
https://...](https://note.com/neko7sora/n/nb0f7820f940e)
<!-- articles feed end -->

---
